﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PinkyMedicalImagesProcessor.Common;

namespace PinkyMedicalImagesProcessor.L02_RoznicaObrazow
{
    public partial class RoznicaObrazowForm : Form
    {
        public RoznicaObrazowForm()
        {
            InitializeComponent();
            pixelzoomPictureBox1.Image = Resource1.DSA1;
            pixelzoomPictureBox2.Image = Resource1.DSA2;
            pixelzoomPictureBox3.Image = GetContrast(Resource1.DSA1, Resource1.DSA2);
        }

        private Image GetContrast(Bitmap img1, Bitmap img2)
        {
            var diffImg = new Bitmap(img1.Width, img1.Height);

            for (int x = 0; x < img1.Width; x++)
            {
                for (int y = 0; y < img1.Height; y++)
                {
                    var c1 = img1.GetPixel(x, y);
                    var c2 = img2.GetPixel(x, y);
                    var factor = 5;
                    var newA = Functions.Truncate((c1.A - c2.A) * factor);
                    var newR = Functions.Truncate((c1.R - c2.R) * factor);
                    var newG = Functions.Truncate((c1.G - c2.G) * factor);
                    var newB = Functions.Truncate((c1.B - c2.B) * factor);

                    var c = Color.FromArgb(newR, newG, newB);
                    diffImg.SetPixel(x, y, c);
                }
            }

            var result = new Bitmap(img1.Width, img1.Height);

            for (int x = 0; x < img1.Width; x++)
            {
                for (int y = 0; y < img1.Height; y++)
                {
                    var c1 = img1.GetPixel(x, y);
                    var c2 = diffImg.GetPixel(x, y);

                    var newR = Functions.Truncate(c1.R - c2.R);
                    var newG = Functions.Truncate(c1.G - c2.G);
                    var newB = Functions.Truncate(c1.B - c2.B);

                    var c = Color.FromArgb(newR, newG, newB);
                    result.SetPixel(x, y, c);
                }
            }

            return result;  
        }
    }
}
