﻿namespace PinkyMedicalImagesProcessor.L02_RoznicaObrazow
{
    partial class RoznicaObrazowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pixelzoomPictureBox1 = new PinkyMedicalImagesProcessor.Common.PixelzoomPictureBox();
            this.pixelzoomPictureBox2 = new PinkyMedicalImagesProcessor.Common.PixelzoomPictureBox();
            this.pixelzoomPictureBox3 = new PinkyMedicalImagesProcessor.Common.PixelzoomPictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.pixelzoomPictureBox3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.pixelzoomPictureBox2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.pixelzoomPictureBox1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(439, 322);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // pixelzoomPictureBox1
            // 
            this.pixelzoomPictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pixelzoomPictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pixelzoomPictureBox1.Name = "pixelzoomPictureBox1";
            this.pixelzoomPictureBox1.Size = new System.Drawing.Size(140, 316);
            this.pixelzoomPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pixelzoomPictureBox1.TabIndex = 1;
            this.pixelzoomPictureBox1.TabStop = false;
            // 
            // pixelzoomPictureBox2
            // 
            this.pixelzoomPictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pixelzoomPictureBox2.Location = new System.Drawing.Point(149, 3);
            this.pixelzoomPictureBox2.Name = "pixelzoomPictureBox2";
            this.pixelzoomPictureBox2.Size = new System.Drawing.Size(140, 316);
            this.pixelzoomPictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pixelzoomPictureBox2.TabIndex = 2;
            this.pixelzoomPictureBox2.TabStop = false;
            // 
            // pixelzoomPictureBox3
            // 
            this.pixelzoomPictureBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pixelzoomPictureBox3.Location = new System.Drawing.Point(295, 3);
            this.pixelzoomPictureBox3.Name = "pixelzoomPictureBox3";
            this.pixelzoomPictureBox3.Size = new System.Drawing.Size(141, 316);
            this.pixelzoomPictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pixelzoomPictureBox3.TabIndex = 3;
            this.pixelzoomPictureBox3.TabStop = false;
            // 
            // RoznicaObrazowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 322);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "RoznicaObrazowForm";
            this.Text = "RoznicaObrazowForm";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Common.PixelzoomPictureBox pixelzoomPictureBox1;
        private Common.PixelzoomPictureBox pixelzoomPictureBox3;
        private Common.PixelzoomPictureBox pixelzoomPictureBox2;
    }
}