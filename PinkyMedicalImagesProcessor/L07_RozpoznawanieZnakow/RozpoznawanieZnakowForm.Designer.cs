﻿namespace PinkyMedicalImagesProcessor.L07_RozpoznawanieZnakow
{
    partial class RozpoznawanieZnakowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.pixelzoomPictureBox1 = new PinkyMedicalImagesProcessor.Common.PixelzoomPictureBox();
            this.pixelzoomPictureBox2 = new PinkyMedicalImagesProcessor.Common.PixelzoomPictureBox();
            this.pixelzoomPictureBox3 = new PinkyMedicalImagesProcessor.Common.PixelzoomPictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoScroll = true;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.pixelzoomPictureBox3, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.pixelzoomPictureBox2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.button4, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button3, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.button2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.button1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.pixelzoomPictureBox1, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(649, 409);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // button4
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.button4, 3);
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.Location = new System.Drawing.Point(3, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(643, 54);
            this.button4.TabIndex = 6;
            this.button4.Text = "Load Patterns";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.Location = new System.Drawing.Point(435, 63);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(211, 54);
            this.button3.TabIndex = 5;
            this.button3.Text = "Do The Magic!";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Location = new System.Drawing.Point(219, 63);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(210, 54);
            this.button2.TabIndex = 4;
            this.button2.Text = "Prepare Image";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(3, 63);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(210, 54);
            this.button1.TabIndex = 3;
            this.button1.Text = "Open File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pixelzoomPictureBox1
            // 
            this.pixelzoomPictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pixelzoomPictureBox1.Location = new System.Drawing.Point(3, 123);
            this.pixelzoomPictureBox1.Name = "pixelzoomPictureBox1";
            this.pixelzoomPictureBox1.Size = new System.Drawing.Size(210, 283);
            this.pixelzoomPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pixelzoomPictureBox1.TabIndex = 7;
            this.pixelzoomPictureBox1.TabStop = false;
            // 
            // pixelzoomPictureBox2
            // 
            this.pixelzoomPictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pixelzoomPictureBox2.Location = new System.Drawing.Point(219, 123);
            this.pixelzoomPictureBox2.Name = "pixelzoomPictureBox2";
            this.pixelzoomPictureBox2.Size = new System.Drawing.Size(210, 283);
            this.pixelzoomPictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pixelzoomPictureBox2.TabIndex = 8;
            this.pixelzoomPictureBox2.TabStop = false;
            // 
            // pixelzoomPictureBox3
            // 
            this.pixelzoomPictureBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pixelzoomPictureBox3.Location = new System.Drawing.Point(435, 123);
            this.pixelzoomPictureBox3.Name = "pixelzoomPictureBox3";
            this.pixelzoomPictureBox3.Size = new System.Drawing.Size(211, 283);
            this.pixelzoomPictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pixelzoomPictureBox3.TabIndex = 9;
            this.pixelzoomPictureBox3.TabStop = false;
            // 
            // RozpoznawanieZnakowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 409);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "RozpoznawanieZnakowForm";
            this.Text = "RozpoznawanieZnakowForm";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private Common.PixelzoomPictureBox pixelzoomPictureBox1;
        private Common.PixelzoomPictureBox pixelzoomPictureBox3;
        private Common.PixelzoomPictureBox pixelzoomPictureBox2;
    }
}