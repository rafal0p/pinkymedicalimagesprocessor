﻿using PinkyMedicalImagesProcessor.Common.OCR;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinkyMedicalImagesProcessor.L07_RozpoznawanieZnakow
{
    public class OcrModel
    {
        OcrImageProcessor ocrProcessor;

        public OcrModel()
        {
            ocrProcessor = new OcrImageProcessor();
        }

        public void ImportImage(Bitmap originalBitmap)
        {
            ocrProcessor.ImportImage(originalBitmap);
            ocrProcessor.Process();
        }

        public Bitmap GetProcessedBitmap()
        {
            var btm = ocrProcessor.GetIntermediateBitmap();
            return btm;
        }

        public Bitmap GetFinalBitmap()
        {
            Bitmap b = ocrProcessor.GetFinalBitmap();
            return b;
        }

        public void ImportLearningSamples(Bitmap learnigSamples)
        {
            ocrProcessor.ImportLearningImages(learnigSamples);
        }
    }
}
