﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PinkyMedicalImagesProcessor.L07_RozpoznawanieZnakow
{
    public partial class RozpoznawanieZnakowForm : Form
    {
        private Bitmap originalBitmap;
        private OcrModel model;
        private Bitmap learnigSamples;

        public RozpoznawanieZnakowForm()
        {
            InitializeComponent();
            model = new OcrModel();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Bitmap files (*.bmp)|*.bmp|PNG files (*.png)|*.png|TIFF files (*.tif)|*tif|JPEG files (*.jpg)|*.jpg |All files (*.*)|*.*";
            ofd.FilterIndex = 5;
            ofd.RestoreDirectory = true;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                originalBitmap = new Bitmap(ofd.FileName);
                pixelzoomPictureBox1.Image = originalBitmap;
            }

            model.ImportImage(originalBitmap);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Bitmap processed = model.GetProcessedBitmap();
            pixelzoomPictureBox2.Image = processed;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Bitmap final = model.GetFinalBitmap();
            pixelzoomPictureBox3.Image = final;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Bitmap files (*.bmp)|*.bmp|PNG files (*.png)|*.png|TIFF files (*.tif)|*tif|JPEG files (*.jpg)|*.jpg |All files (*.*)|*.*";
            ofd.FilterIndex = 5;
            ofd.RestoreDirectory = true;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                learnigSamples = new Bitmap(ofd.FileName);
            }

            model.ImportLearningSamples(learnigSamples);
        }
    }
}
