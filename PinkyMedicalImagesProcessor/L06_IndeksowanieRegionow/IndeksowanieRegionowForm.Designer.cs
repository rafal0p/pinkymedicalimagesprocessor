﻿namespace PinkyMedicalImagesProcessor.L06_IndeksowanieRegionow
{
    partial class IndeksowanieRegionowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pixelzoomPictureBox1 = new PinkyMedicalImagesProcessor.Common.PixelzoomPictureBox();
            this.pixelzoomPictureBox2 = new PinkyMedicalImagesProcessor.Common.PixelzoomPictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.calculateButton = new System.Windows.Forms.Button();
            this.thresholdTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.pixelzoomPictureBox2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.pixelzoomPictureBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(726, 510);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // pixelzoomPictureBox1
            // 
            this.pixelzoomPictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pixelzoomPictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pixelzoomPictureBox1.Name = "pixelzoomPictureBox1";
            this.pixelzoomPictureBox1.Size = new System.Drawing.Size(357, 384);
            this.pixelzoomPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pixelzoomPictureBox1.TabIndex = 0;
            this.pixelzoomPictureBox1.TabStop = false;
            // 
            // pixelzoomPictureBox2
            // 
            this.pixelzoomPictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pixelzoomPictureBox2.Location = new System.Drawing.Point(366, 3);
            this.pixelzoomPictureBox2.Name = "pixelzoomPictureBox2";
            this.pixelzoomPictureBox2.Size = new System.Drawing.Size(357, 384);
            this.pixelzoomPictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pixelzoomPictureBox2.TabIndex = 1;
            this.pixelzoomPictureBox2.TabStop = false;
            // 
            // panel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 2);
            this.panel1.Controls.Add(this.calculateButton);
            this.panel1.Controls.Add(this.thresholdTextBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 393);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(720, 114);
            this.panel1.TabIndex = 2;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(212, 3);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(88, 23);
            this.calculateButton.TabIndex = 19;
            this.calculateButton.Text = "Show";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // thresholdTextBox
            // 
            this.thresholdTextBox.Location = new System.Drawing.Point(150, 3);
            this.thresholdTextBox.Name = "thresholdTextBox";
            this.thresholdTextBox.Size = new System.Drawing.Size(56, 22);
            this.thresholdTextBox.TabIndex = 18;
            this.thresholdTextBox.Text = "5";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(84, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 17);
            this.label1.TabIndex = 17;
            this.label1.Text = "threshold:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "Open file";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // IndeksowanieRegionowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(726, 510);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "IndeksowanieRegionowForm";
            this.Text = "IndeksowanieRegionowForm";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Common.PixelzoomPictureBox pixelzoomPictureBox1;
        private Common.PixelzoomPictureBox pixelzoomPictureBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.TextBox thresholdTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
    }
}