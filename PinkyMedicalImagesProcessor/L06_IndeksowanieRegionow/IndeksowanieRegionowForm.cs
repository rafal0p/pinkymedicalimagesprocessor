﻿using PinkyMedicalImagesProcessor.Common.CCL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PinkyMedicalImagesProcessor.L06_IndeksowanieRegionow
{
    public partial class IndeksowanieRegionowForm : Form
    {
        private Bitmap loadedBitmap;

        public IndeksowanieRegionowForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog() { RestoreDirectory = true };
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                loadedBitmap = new Bitmap(ofd.FileName);
                pixelzoomPictureBox1.Image = loadedBitmap;
                CalculateEdges();
            }
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            CalculateEdges();
        }

        private void CalculateEdges()
        {
            var threshold = int.Parse(this.thresholdTextBox.Text);
            var ccl = new CclImageProcessor();
            ccl.ImportImage(loadedBitmap);
            ccl.Process(threshold);
            var resultBitmap = ccl.GetResult();
            pixelzoomPictureBox2.Image = resultBitmap;
        }
    }
}
