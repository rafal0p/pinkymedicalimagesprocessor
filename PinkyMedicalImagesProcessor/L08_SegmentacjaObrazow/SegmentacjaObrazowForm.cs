﻿using PinkyMedicalImagesProcessor.Common.Otsu;
using PinkyMedicalImagesProcessor.Common.Watershed;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PinkyMedicalImagesProcessor.L08_SegmentacjaObrazow
{
    public partial class SegmentacjaObrazowForm : Form
    {
        public SegmentacjaObrazowForm()
        {
            InitializeComponent();
        }

        private void SegmentacjaObrazowForm_Load(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog() { RestoreDirectory = true };
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                var loadedBitmap = new Bitmap(ofd.FileName);
                var watershed = new WatershedGrayscale();
                watershed.BorderInWhite = true;
                //var otsu = new OtsuImageProcessor();
                //otsu.ImportImage(loadedBitmap);
                //var otsuBitmap = otsu.GetResult();
                var resultBitmap = watershed.GetResult(loadedBitmap);
                pixelzoomPictureBox1.Image = resultBitmap;
            }
        }
    }
}
