﻿namespace PinkyMedicalImagesProcessor.L08_SegmentacjaObrazow
{
    partial class SegmentacjaObrazowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pixelzoomPictureBox1 = new PinkyMedicalImagesProcessor.Common.PixelzoomPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pixelzoomPictureBox1
            // 
            this.pixelzoomPictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pixelzoomPictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pixelzoomPictureBox1.Name = "pixelzoomPictureBox1";
            this.pixelzoomPictureBox1.Size = new System.Drawing.Size(282, 253);
            this.pixelzoomPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pixelzoomPictureBox1.TabIndex = 0;
            this.pixelzoomPictureBox1.TabStop = false;
            // 
            // SegmentacjaObrazowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Controls.Add(this.pixelzoomPictureBox1);
            this.Name = "SegmentacjaObrazowForm";
            this.Text = "SegmentacjaObrazowForm";
            this.Load += new System.EventHandler(this.SegmentacjaObrazowForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.PixelzoomPictureBox pixelzoomPictureBox1;
    }
}