﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PinkyMedicalImagesProcessor.L03_ProgowanieObrazowOld
{
    public partial class ProgowanieObrazowForm : Form
    {
        private int width = 512;
        private int height = 828;
        private int[,] img1;

        public ProgowanieObrazowForm()
        {
            InitializeComponent();
            Bitmap b = ImportImage(@"C:\Incoming\img\xrayimg\673.xray");
            pixelzoomPictureBox1.Image = b;
        }

        private Bitmap ImportImage(string s)
        {
            var result = new Bitmap(width, height);
            img1 = new int[height, width];
            int[] img1d1 = Get1dImage(@"C:\Incoming\img\xrayimg\672.xray");
            int[] img1d2 = Get1dImage(@"C:\Incoming\img\xrayimg\673.xray");

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    var val1 = img1d1[y * width + x];
                    var val2 = img1d2[y * width + x];
                    img1[y, x] = val2 - val1;
                }
            }
            return GetBitmap(img1);
        }

        private int[] Get1dImage(string s)
        {
            var img1d = new int[height * width];
            using (var fs = new FileStream(s, FileMode.Open))
            {
                byte[] b = new byte[2];
                for (int i = 0; i < img1d.Length; i++)
                {
                    //fs.Read(b, 0, 2);
                    //var val = BitConverter.ToUInt16(b, 0);
                    //img1d[i] = val;
                    fs.ReadByte();
                    img1d[i] = fs.ReadByte();
                }
            }

            return img1d;
        }

        protected Bitmap GetBitmap(int[,] img)
        {
            var bitmap = new Bitmap(width, height);
            var bytesPerPixel = 4;
            var bitmapData1 = bitmap.LockBits(new Rectangle(0, 0, width, height),
                                     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            unsafe
            {
                byte* ptr = (byte*)bitmapData1.Scan0;
                for (int y = 0; y < bitmapData1.Height; y++)
                {
                    for (int x = 0; x < bitmapData1.Width; x++)
                    {
                        ptr[0] = (byte)(img[y, x]);
                        ptr[1] = (byte)(img[y, x]);
                        ptr[2] = (byte)(img[y, x]);
                        ptr[3] = (byte)(img[y, x]);
                        ptr += bytesPerPixel;
                    }
                    ptr += (bitmapData1.Stride - (bitmapData1.Width * bytesPerPixel));
                }
            }
            bitmap.UnlockBits(bitmapData1);
            return bitmap;
        }
    }
}
