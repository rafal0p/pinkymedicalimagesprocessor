﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PinkyMedicalImagesProcessor
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var l2 = new L02_RoznicaObrazow.RoznicaObrazowForm();
            l2.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var l3 = new L03_ProgowanieObrazow.ProgowanieObrazowForm();
            l3.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var l4 = new L04_OdszukiwanieKrawedzi.OdszukiwanieKrawedziForm();
            l4.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var l5 = new L05_Szkieletyzacja.SzkieletyzacjaForm();
            l5.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var l6 = new L06_IndeksowanieRegionow.IndeksowanieRegionowForm();
            l6.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var l7 = new L07_RozpoznawanieZnakow.RozpoznawanieZnakowForm();
            l7.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            var l8 = new L08_SegmentacjaObrazow.SegmentacjaObrazowForm();
            l8.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            var l9 = new L09_WyszukiwanieProstych.WyszukiwanieProstychForm();
            l9.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            var l11 = new L11_ObslugaDicom.DicomForm();
            l11.Show();
        }

        private void button11_Click(object sender, EventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {

        }
    }
}
