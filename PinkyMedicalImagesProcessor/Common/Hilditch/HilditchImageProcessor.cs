﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinkyMedicalImagesProcessor.Common.Hilditch
{
    public class HilditchImageProcessor : ImageProcessorBase
    {
        private int[,] modifiedImg;
        private const byte ON = 255;
        private const byte OFF = 0;

        public override Bitmap GetResult()
        {
            BinarizeImage(150);
            ApplyHilditchAlghoritm();
            return GetBitmap(modifiedImg);
        }

        public void ApplyHilditchAlghoritm()
        {
            modifiedImg = CopyImage<int, int>(binarizedImage);
            int a, b;
            bool changed;
            do
            {
                changed = false;
                for (int y = 2; y < height - 2; y++)
                {
                    for (int x = 2; x < width - 2; x++)
                    {
                        a = GetFactorA(y, x);
                        b = GetFactorB(y, x);
                        var c1 = modifiedImg[y, x] == ON;
                        var c2 = 2 <= b;
                        var c3 = b <= 6;
                        var c4 = a == 1;
                        var c5 = (modifiedImg[y - 1, x] == OFF &&
                                  modifiedImg[y, x + 1] == OFF &&
                                  modifiedImg[y, x - 1] == OFF) || (GetFactorA(y - 1, x) != 1);
                        var c6 = (modifiedImg[y - 1, x] == OFF &&
                                  modifiedImg[y, x + 1] == OFF &&
                                  modifiedImg[y + 1, x] == OFF) || (GetFactorA(y, x + 1) != 1);
                        if (c1 && c2 && c3 && c4/* && c5 && c6*/)
                        {
                            modifiedImg[y, x] = OFF;
                            changed = true;
                        }
                    }
                }
            } while (changed);
        }

        private int GetFactorA(int y, int x)
        {
            var count = 0;
            if (y - 1 >= 0 && x + 1 < width && modifiedImg[y - 1, x] == OFF && modifiedImg[y - 1, x + 1] == ON)
                count++;
            if (y - 1 >= 0 && x + 1 < width && modifiedImg[y - 1, x + 1] == OFF && modifiedImg[y, x + 1] == ON)
                count++;
            if (y + 1 < height && x + 1 < width && modifiedImg[y, x + 1] == OFF && modifiedImg[y + 1, x + 1] == ON)
                count++;
            if (y + 1 < height && x + 1 < width && modifiedImg[y + 1, x + 1] == OFF && modifiedImg[y + 1, x] == ON)
                count++;
            if (y + 1 < height && x - 1 >= 0 && modifiedImg[y + 1, x] == OFF && modifiedImg[y + 1, x - 1] == ON)
                count++;
            if (y + 1 < height && x - 1 >= 0 && modifiedImg[y + 1, x - 1] == OFF && modifiedImg[y, x - 1] == ON)
                count++;
            if (y - 1 >= 0 && x - 1 >= 0 && modifiedImg[y, x - 1] == OFF && modifiedImg[y - 1, x - 1] == ON)
                count++;
            if (y - 1 >= 0 && x - 1 >= 0 && modifiedImg[y - 1, x - 1] == OFF && modifiedImg[y - 1, x] == ON)
                count++;

            return count;
        }

        private int GetFactorB(int y, int x)
        {
            var result = 0;
            if (modifiedImg[y - 1, x] == ON)
                result++;
            if (modifiedImg[y - 1, x + 1] == ON)
                result++;
            if (modifiedImg[y, x + 1] == ON)
                result++;
            if (modifiedImg[y + 1, x + 1] == ON)
                result++;
            if (modifiedImg[y + 1, x] == ON)
                result++;
            if (modifiedImg[y + 1, x - 1] == ON)
                result++;
            if (modifiedImg[y, x - 1] == ON)
                result++;
            if (modifiedImg[y - 1, x - 1] == ON)
                result++;
            return result;
        }
    }
}
