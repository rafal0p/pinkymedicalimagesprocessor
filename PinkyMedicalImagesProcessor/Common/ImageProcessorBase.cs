﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinkyMedicalImagesProcessor.Common
{
    public class ImageProcessorBase
    {
        protected int[,] grayImage;
        protected int height;
        protected int width;
        protected int[,] binarizedImage;
        protected Bitmap inputBitmap;

        public virtual Bitmap GetResult()
        {
            BinarizeImage(200);
            return GetBitmap(binarizedImage);
        }

        public void ImportImage(Bitmap bitmap)
        {
            inputBitmap = new Bitmap(bitmap);
            width = bitmap.Width;
            height = bitmap.Height;

            grayImage = GetImageArrayFromBitmap(bitmap);
        }

        protected void BinarizeImage(int binarizeTresh)
        {
            binarizedImage = new int[height, width];
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (grayImage[y, x] < binarizeTresh)
                        binarizedImage[y, x] = 0;
                    else
                        binarizedImage[y, x] = 255;
                }
            }
        }

        protected int[,] GetImageArrayFromBitmap(Bitmap bitmap)
        {
            var localWidth = bitmap.Width;
            var localHeight = bitmap.Height;
            var resultImage = new int[localHeight, localWidth];
            var bytesPerPixel = 4;
            var bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                                     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            unsafe
            {
                byte* ptr = (byte*)bitmapData.Scan0;
                for (int y = 0; y < bitmapData.Height; y++)
                {
                    for (int x = 0; x < bitmapData.Width; x++)
                    {
                        resultImage[y, x] = GetGrayFromRGB(ptr[0], ptr[1], ptr[2]);
                        ptr += bytesPerPixel;
                    }
                    ptr += bitmapData.Stride - (bitmapData.Width * bytesPerPixel);
                }
            }
            bitmap.UnlockBits(bitmapData);

            return resultImage;
        }

        public Bitmap GetBitmap(int[,] img)
        {
            var bitmap = new Bitmap(width, height);
            var bytesPerPixel = 4;
            var bitmapData1 = bitmap.LockBits(new Rectangle(0, 0, width, height),
                                     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            unsafe
            {
                byte* ptr = (byte*)bitmapData1.Scan0;
                for (int y = 0; y < bitmapData1.Height; y++)
                {
                    for (int x = 0; x < bitmapData1.Width; x++)
                    {
                        ptr[0] = GetB(img[y, x]);
                        ptr[1] = GetG(img[y, x]);
                        ptr[2] = GetR(img[y, x]);
                        ptr[3] = GetA(img[y, x]);
                        ptr += bytesPerPixel;
                    }
                    ptr += (bitmapData1.Stride - (bitmapData1.Width * bytesPerPixel));
                }
            }
            bitmap.UnlockBits(bitmapData1);
            return bitmap;
        }

        public Bitmap GetBitmap(float[,] img)
        {
            var bitmap = new Bitmap(width, height);
            var bytesPerPixel = 4;
            var bitmapData1 = bitmap.LockBits(new Rectangle(0, 0, width, height),
                                     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            unsafe
            {
                byte* ptr = (byte*)bitmapData1.Scan0;
                for (int y = 0; y < bitmapData1.Height; y++)
                {
                    for (int x = 0; x < bitmapData1.Width; x++)
                    {
                        ptr[0] = (byte)img[y, x];
                        ptr[1] = (byte)img[y, x];
                        ptr[2] = (byte)img[y, x];
                        ptr[3] = (byte)img[y, x];
                        ptr += bytesPerPixel;
                    }
                    ptr += (bitmapData1.Stride - (bitmapData1.Width * bytesPerPixel));
                }
            }
            bitmap.UnlockBits(bitmapData1);
            return bitmap;
        }

        protected void ForEachPixel(Action<int, int> f, int limit = 0)
        {
            for (int y = limit; y <= ((height - 1) - limit); y++)
            {
                for (int x = limit; x <= ((width - 1) - limit); x++)
                {
                    f(y, x);
                }
            }
        }

        protected TResult[,] CopyImage<TSource, TResult>(TSource[,] img)
        {
            var result = new TResult[img.GetLength(0), img.GetLength(1)];
            for (int j = 0; j < result.GetLength(0); j++)
            {
                for (int i = 0; i < result.GetLength(1); i++)
                {
                    result[j, i] = (TResult)Convert.ChangeType(img[j, i], typeof(TResult));
                }
            }
            return result;
        }

        protected virtual byte GetR(int val)
        {
            return (byte)val;
        }

        protected virtual byte GetG(int val)
        {
            return (byte)val;
        }

        protected virtual byte GetB(int val)
        {
            return (byte)val;
        }

        protected virtual byte GetA(int val)
        {
            return (byte)255;
        }

        protected int GetGrayFromRGB(byte v1, byte v2, byte v3)
        {
            return (v1 + v2 + v3) / 3;
        }
    }
}
