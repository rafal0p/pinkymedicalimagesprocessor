﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace PinkyMedicalImagesProcessor.Common.Canny
{
    public class CannyImageProcessor : ImageProcessorBase
    {
        private int kernelWeight;
        
        private int[,] GaussianKernel;
        public int halfKernelSize
        {
            get { return KernelSize / 2; }
        }

        public int[,] GrayImage { get; private set; }
        public float[,] DerivativeX { get; private set; }
        public float[,] DerivativeY { get; private set; }
        public int[,] FilteredImage { get; private set; }
        public float[,] Gradient { get; private set; }
        public float[,] NonMax { get; private set; }
        public int[,] PostHysteresis { get; private set; }
        public int[,] EdgePoints { get; private set; }
        public float[,] GNH { get; private set; }
        public float[,] GNL { get; private set; }
        public int[,] EdgeMap { get; private set; }
        public int[,] VisitedMap { get; private set; }

        public float Sigma { get; set; }
        public float MaxHysteresisThresh { get; set; }
        public float MinHysteresisThresh { get; set; }
        public int KernelSize { get; set; }

        public override Bitmap GetResult()
        {
            GrayImage = grayImage;
            DetectCannyEdges();
            return GetBitmap(EdgeMap);
        }

        public CannyImageProcessor()
        {
            MaxHysteresisThresh = 20F;
            MinHysteresisThresh = 10F;
            Sigma = 1;
            KernelSize = 5;
        }

        private void DetectCannyEdges()
        {
            CreateImgs();
            FilteredImage = GaussianFilter(GrayImage);

            int[,] sobelX = {{1,0,-1},
                         {2,0,-2},
                         {1,0,-1}};

            int[,] sobelY = {{1,2,1},
                         {0,0,0},
                         {-1,-2,-1}};

            DerivativeX = ApplyFilter(FilteredImage, sobelX);
            DerivativeY = ApplyFilter(FilteredImage, sobelY);
            Gradient = FillGradient();
            NonMax = FillNonMax();
            PostHysteresis = FillPostHysteresis();
            FillEdgePointsGnhAndGnl();
            HysterisisThresholding();
            ChangeOnesTo255();
        }

        private void ChangeOnesTo255()
        {
            ForEachPixel((y, x) => EdgeMap[y, x] = EdgeMap[y, x] * 255);
        }

        private void FillEdgePointsGnhAndGnl()
        {
            ForEachPixel((y, x) =>
            {
                if (PostHysteresis[y, x] >= MaxHysteresisThresh)
                {
                    EdgePoints[y, x] = 1;
                    GNH[y, x] = 255;
                }
                if ((PostHysteresis[y, x] < MaxHysteresisThresh) && (PostHysteresis[y, x] >= MinHysteresisThresh))
                {
                    EdgePoints[y, x] = 2;
                    GNL[y, x] = 255;
                }
            }, halfKernelSize);
        }

        private float[,] FillGradient()
        {
            var result = new float[height, width];
            ForEachPixel((y, x) =>
                result[y, x] = (float)Math.Sqrt(
                    (DerivativeX[y, x] * DerivativeX[y, x]) + (DerivativeY[y, x] * DerivativeY[y, x])));
            return result;
        }

        private int[,] FillPostHysteresis()
        {
            var result = new int[height, width];
            ForEachPixel((y, x) => result[y, x] = (int)NonMax[y, x], halfKernelSize);
            return result;
        }

        private float[,] FillNonMax()
        {
            var result = CopyImage<float, float>(Gradient);
            float tangent;
            ForEachPixel((y, x) =>
            {
                if (DerivativeX[y, x] == 0)
                    tangent = 90f;
                else
                    tangent = RadToDeg(Math.Atan(DerivativeY[y, x] / DerivativeX[y, x]));

                if (((-22.5 < tangent) && (tangent <= 22.5)) || ((157.5 < tangent) && (tangent <= -157.5)))
                {
                    if ((Gradient[y, x] < Gradient[y, x + 1]) || (Gradient[y, x] < Gradient[y, x - 1]))
                        result[y, x] = 0;
                }
                if (((-112.5 < tangent) && (tangent <= -67.5)) || ((67.5 < tangent) && (tangent <= 112.5)))
                {
                    if ((Gradient[y, x] < Gradient[y + 1, x]) || (Gradient[y, x] < Gradient[y - 1, x]))
                        result[y, x] = 0;
                }
                if (((-67.5 < tangent) && (tangent <= -22.5)) || ((112.5 < tangent) && (tangent <= 157.5)))
                {
                    if ((Gradient[y, x] < Gradient[y + 1, x - 1]) || (Gradient[y, x] < Gradient[y - 1, x + 1]))
                        result[y, x] = 0;
                }
                if (((-157.5 < tangent) && (tangent <= -112.5)) || ((67.5 < tangent) && (tangent <= 22.5)))
                {
                    if ((Gradient[y, x] < Gradient[y + 1, x + 1]) || (Gradient[y, x] < Gradient[y - 1, x - 1]))
                        result[y, x] = 0;
                }
            }, halfKernelSize);
            return result;
        }

        private void HysterisisThresholding()
        {
            ForEachPixel((y, x) =>
            {
                if (EdgePoints[y, x] == 1)
                {
                    EdgeMap[y, x] = 1;
                    Travers(y, x);
                    VisitedMap[y, x] = 1;
                }
            }, halfKernelSize);
        }

        private void Travers(int y, int x)
        {
            if (VisitedMap[y, x] == 1)
            {
                return;
            }

            if (EdgePoints[y + 1, x] == 2)
            {
                EdgeMap[y + 1, x] = 1;
                VisitedMap[y + 1, x] = 1;
                Travers(y + 1, x);
                return;
            }
            if (EdgePoints[y + 1, x - 1] == 2)
            {
                EdgeMap[y + 1, x - 1] = 1;
                VisitedMap[y + 1, x - 1] = 1;
                Travers(y + 1, x - 1);
                return;
            }
            if (EdgePoints[y, x - 1] == 2)
            {
                EdgeMap[y, x - 1] = 1;
                VisitedMap[y, x - 1] = 1;
                Travers(y, x - 1);
                return;
            }
            if (EdgePoints[y - 1, x - 1] == 2)
            {
                EdgeMap[y - 1, x - 1] = 1;
                VisitedMap[y - 1, x - 1] = 1;
                Travers(y - 1, x - 1);
                return;
            }
            if (EdgePoints[y - 1, x] == 2)
            {
                EdgeMap[y - 1, x] = 1;
                VisitedMap[y - 1, x] = 1;
                Travers(y - 1, x);
                return;
            }
            if (EdgePoints[y - 1, x + 1] == 2)
            {
                EdgeMap[y - 1, x + 1] = 1;
                VisitedMap[y - 1, x + 1] = 1;
                Travers(y - 1, x + 1);
                return;
            }
            if (EdgePoints[y, x + 1] == 2)
            {
                EdgeMap[y, x + 1] = 1;
                VisitedMap[y, x + 1] = 1;
                Travers(y, x + 1);
                return;
            }
            if (EdgePoints[y + 1, x + 1] == 2)
            {
                EdgeMap[y + 1, x + 1] = 1;
                VisitedMap[y + 1, x + 1] = 1;
                Travers(y + 1, x + 1);
                return;
            }
        }

        private float[,] ApplyFilter(int[,] img, int[,] filter)
        {
            var filterWidth = filter.GetLength(0);
            float sum = 0;
            float[,] result = new float[height, width];
            ForEachPixel((y, x) =>
            {
                sum = 0;
                for (int j = -filterWidth / 2; j <= filterWidth / 2; j++)
                {
                    for (int i = -filterWidth / 2; i <= filterWidth / 2; i++)
                    {
                        sum += img[y + j, x + i] * filter[filterWidth / 2 + j, filterWidth / 2 + i];
                    }
                }
                result[y, x] = sum;
            }, filterWidth / 2);
            return result;
        }

        private int[,] GaussianFilter(int[,] img)
        {
            GenerateGaussianKernel();
            float sum = 0;
            var result = CopyImage<int, int>(img);
            ForEachPixel((y, x) =>
            {
                sum = 0;
                for (int j = -halfKernelSize; j <= halfKernelSize; j++)
                {
                    for (int i = -halfKernelSize; i <= halfKernelSize; i++)
                    {
                        sum += ((float)result[y + j, x + i] * GaussianKernel[halfKernelSize + j, halfKernelSize + i]);
                    }
                }
                result[y, x] = (int)(Math.Round(sum / kernelWeight));
            }, halfKernelSize);
            return result;
        }

        public void GenerateGaussianKernel()
        {
            var pi = (float)Math.PI;
            int j, i;

            var kernel = new float[KernelSize, KernelSize];
            GaussianKernel = new int[KernelSize, KernelSize];
            var op = new float[KernelSize, KernelSize];
            float d1, d2;


            d1 = 1 / (2 * pi * Sigma * Sigma);
            d2 = 2 * Sigma * Sigma;

            float min = 1000;

            for (j = -halfKernelSize; j <= halfKernelSize; j++)
            {
                for (i = -halfKernelSize; i <= halfKernelSize; i++)
                {
                    kernel[halfKernelSize + j, halfKernelSize + i] = ((1 / d1) * (float)Math.Exp(-(j * j + i * i) / d2));
                    if (kernel[halfKernelSize + j, halfKernelSize + i] < min)
                        min = kernel[halfKernelSize + j, halfKernelSize + i];

                }
            }
            int mult = (int)(1 / min);
            int sum = 0;
            if ((min > 0) && (min < 1))
            {

                for (j = -halfKernelSize; j <= halfKernelSize; j++)
                {
                    for (i = -halfKernelSize; i <= halfKernelSize; i++)
                    {
                        kernel[halfKernelSize + j, halfKernelSize + i] = (float)Math.Round(kernel[halfKernelSize + j, halfKernelSize + i] * mult, 0);
                        GaussianKernel[halfKernelSize + j, halfKernelSize + i] = (int)kernel[halfKernelSize + j, halfKernelSize + i];
                        sum = sum + GaussianKernel[halfKernelSize + j, halfKernelSize + i];
                    }
                }
            }
            else
            {
                sum = 0;
                for (j = -halfKernelSize; j <= halfKernelSize; j++)
                {
                    for (i = -halfKernelSize; i <= halfKernelSize; i++)
                    {
                        kernel[halfKernelSize + j, halfKernelSize + i] = (float)Math.Round(kernel[halfKernelSize + j, halfKernelSize + i], 0);
                        GaussianKernel[halfKernelSize + j, halfKernelSize + i] = (int)kernel[halfKernelSize + j, halfKernelSize + i];
                        sum = sum + GaussianKernel[halfKernelSize + j, halfKernelSize + i];
                    }
                }
            }
            kernelWeight = sum;
        }

        private float RadToDeg(double rad)
        {
            return (float)(rad * 180 / Math.PI);
        }

        private void CreateImgs()
        {
            EdgeMap = new int[height, width];
            VisitedMap = new int[height, width];

            DerivativeX = new float[height, width];
            DerivativeY = new float[height, width];

            GNH = new float[height, width];
            GNL = new float[height, width]; ;
            EdgePoints = new int[height, width];
        }
    }
}
