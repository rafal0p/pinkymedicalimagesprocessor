﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinkyMedicalImagesProcessor.Common.CCL
{
    public class MyPoint
    {
        public int X { get; private set; }
        public int Y { get; private set; }
        public byte Value { get; private set; }

        public MyPoint(int x, int y, byte value)
        {
            this.X = x;
            this.Y = y;
            this.Value = value;
        }

        public override string ToString()
        {
            return string.Format("{0}:{1}, {2}", X, Y, Value);
        }
    }
}
