﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinkyMedicalImagesProcessor.Common.CCL
{
    public class CclImageProcessor : ImageProcessorBase
    {
        private int[,] regions;
        public IDictionary<int, IList<MyPoint>> Patterns { get; private set; }

        public override Bitmap GetResult()
        {
            return GetBitmap(regions);
        }

        public void Process(int threshold = 120)
        {
            BinarizeImage(threshold);
            regions = new int[height, width];
            var allLabels = new Dictionary<int, Label>();

            FirstStep(allLabels);
            SecondStep(allLabels);
        }

        private void SecondStep(Dictionary<int, Label> allLabels)
        {
            Patterns = new Dictionary<int, IList<MyPoint>>();
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    var patternId = regions[y, x];
                    if (patternId == 0) continue;
                    patternId = allLabels[patternId].GetRoot().Id;
                    regions[y, x] = patternId;

                    if (!Patterns.ContainsKey(patternId))
                        Patterns[patternId] = new List<MyPoint>();

                    Patterns[patternId].Add(new MyPoint(x, y, (byte)grayImage[y, x]));
                }
            }
        }

        private void FirstStep(Dictionary<int, Label> allLabels)
        {
            int labelCount = 1;
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (binarizedImage[y, x] == 0) continue;

                    var neighboringLabels = GetNeighboringLabels(y, x);
                    int currentLabel;

                    if (!neighboringLabels.Any())
                    {
                        currentLabel = labelCount;
                        allLabels.Add(currentLabel, new Label(currentLabel));
                        labelCount++;
                    }
                    else
                    {
                        currentLabel = neighboringLabels.Min(l => allLabels[l].GetRoot().Id);
                        var root = allLabels[currentLabel].GetRoot();

                        foreach (var neighbour in neighboringLabels)
                        {
                            if (root.Id != allLabels[neighbour].GetRoot().Id)
                            {
                                allLabels[neighbour].Join(allLabels[currentLabel]);
                            }
                        }
                    }

                    regions[y, x] = currentLabel;
                }
            }
        }

        protected override byte GetR(int val)
        {
            return (byte)(val * 50000);
        }

        protected override byte GetB(int val)
        {
            return (byte)(val * 500);
        }

        protected override byte GetG(int val)
        {
            return (byte)(val * 5000000);
        }

        private IEnumerable<int> GetNeighboringLabels(int y, int x)
        {
            var neighboringLabels = new List<int>();

            for (int i = y - 1; i <= y + 2 && i < height - 1; i++)
            {
                for (int j = x - 1; j <= x + 2 && j < width - 1; j++)
                {
                    if (i > -1 && j > -1 && regions[i, j] != 0)
                    {
                        neighboringLabels.Add(regions[i, j]);
                    }
                }
            }

            return neighboringLabels;
        }
    }
}
