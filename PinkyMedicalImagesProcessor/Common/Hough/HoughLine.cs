﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinkyMedicalImagesProcessor.Common.Hough
{
    public class HoughLine
    {
        private double theta;
        private double r;
        private byte color = 128;

        public HoughLine(double theta, double r)
        {
            this.theta = theta;
            this.r = r;
        }

        public void Draw(int[,] img)
        {
            var height = img.GetLength(0);
            var width = img.GetLength(1);

            var houghHeight = (int)(Math.Sqrt(2) * Math.Max(height, width)) / 2;

            var centerX = width / 2.0;
            var centerY = height / 2.0;

            var tsin = Math.Sin(theta);
            var tcos = Math.Cos(theta);

            if (theta < Math.PI * 0.25 || theta > Math.PI * 0.75)
            {
                for (int y = 0; y < height; y++)
                {
                    var x = (int)((((r - houghHeight) - ((y - centerY) * tsin)) / tcos) + centerX);
                    if (x < width && x >= 0)
                    {
                        img[y, x] = color;
                    }
                }
            }
            else
            {
                for (int x = 0; x < width; x++)
                {
                    var y = (int)((((r - houghHeight) - ((x - centerX) * tcos)) / tsin) + centerY);
                    if (y < height && y >= 0)
                    {
                        img[y, x] = color;
                    }
                }
            }
        }
    }
}
