﻿using PinkyMedicalImagesProcessor.Common.Canny;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinkyMedicalImagesProcessor.Common.Hough
{
    public class HoughImageProcessor : ImageProcessorBase
    {
        public int Threshold { get; set; }
        public int MaxThresh { get; set; }
        public int MinThresh { get; set; }
        public float Sigma { get; set; }
        public bool UseCanny { get; set; }

        private int neighbourhoodSize = 4;
        private int maxTheta = 180;
        private double thetaStep { get { return Math.PI / maxTheta; } }
        private int[,] houghArray;
        private double centerX, centerY;
        private int houghHeight;
        private int doubleHeight { get { return houghHeight * 2; } }
        private int numPoints;
        private double[] sinCache;
        private double[] cosCache;

        private CannyImageProcessor cannyImgProcessor = new CannyImageProcessor();

        public override Bitmap GetResult()
        {
            int[,] img;
            if (!UseCanny)
            {
                cannyImgProcessor.ImportImage(this.inputBitmap);
                cannyImgProcessor.MaxHysteresisThresh = MaxThresh;
                cannyImgProcessor.MinHysteresisThresh = MinThresh;
                cannyImgProcessor.Sigma = Sigma;
                cannyImgProcessor.GetResult();
                img = cannyImgProcessor.EdgeMap;
            }
            else
            {
                img = this.grayImage;
            }
            Initialize();
            AddPoints(img);

            var lines = GetLines(Threshold);
            foreach (var line in lines)
            {
                line.Draw(img);
            }

            return GetBitmap(img);
        }

        private void Initialize()
        {
            houghHeight = (int)(Math.Sqrt(2) * Math.Max(width, height)) / 2;
            houghArray = new int[maxTheta, doubleHeight];
            centerX = width / 2;
            centerY = height / 2;

            FillSinAndCosCaches();
        }

        private void AddPoints(int[,] img)
        {
            ForEachPixel((y, x) =>
            {
                if (img[y, x] != 0)
                    AddPoint(y, x);
            });
        }

        private void AddPoint(int y, int x)
        {
            for (int t = 0; t < maxTheta; t++)
            {
                var r = (int)(((x - centerX) * cosCache[t]) + ((y - centerY) * sinCache[t]));
                r += houghHeight;
                if (r < 0 || r >= doubleHeight) continue;

                houghArray[t, r]++;
            }

            numPoints++;
        }

        public IList<HoughLine> GetLines(int threshold)
        {
            var lines = new List<HoughLine>();
            if (numPoints != 0)
            {
                for (int t = 0; t < maxTheta; t++)
                {
                    for (int r = neighbourhoodSize; r < doubleHeight - neighbourhoodSize; r++)
                    {
                        if (houghArray[t, r] > threshold)
                        {
                            var peak = houghArray[t, r];
                            for (int dx = -neighbourhoodSize; dx <= neighbourhoodSize; dx++)
                            {
                                for (int dy = -neighbourhoodSize; dy < neighbourhoodSize; dy++)
                                {
                                    var dt = t + dx;
                                    var dr = r + dy;
                                    dt = NormalizeAngle(dt);

                                    if (houghArray[dt, dr] > peak)
                                    {
                                        goto loop;
                                        //break;
                                    }
                                }
                            }

                            var theta = t * thetaStep;
                            lines.Add(new HoughLine(theta, r));
                            loop:;
                        }
                    }
                }
            }
            return lines;
        }

        private int NormalizeAngle(int dt)
        {
            if (dt < 0)
                dt += maxTheta;
            else if (dt >= maxTheta)
                dt -= maxTheta;
            return dt;
        }

        protected override byte GetR(int val)
        {
            if (val == 128)
                return (byte)255;
            return (byte)val;
        }

        private void FillSinAndCosCaches()
        {
            sinCache = new double[maxTheta];
            cosCache = new double[maxTheta];
            for (int t = 0; t < maxTheta; t++)
            {
                var realTheta = t * thetaStep;
                sinCache[t] = Math.Sin(realTheta);
                cosCache[t] = Math.Cos(realTheta);
            }
        }
    }
}
