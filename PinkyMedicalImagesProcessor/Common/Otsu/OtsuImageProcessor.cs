﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinkyMedicalImagesProcessor.Common.Otsu
{
    public class OtsuImageProcessor : ImageProcessorBase
    {
        private int[] simpleHisto;
        private int[,] improvedHisto;

        public bool SimpleOtsuEnabled { get; set; }

        public override Bitmap GetResult()
        {
            simpleHisto = GetHistogram(grayImage);
            improvedHisto = Get2DHistogram(grayImage);

            var treshold = SimpleOtsu(simpleHisto, grayImage.Length);
            var improvedThreshold = ImprovedOtsu(improvedHisto, grayImage.Length);

            if (SimpleOtsuEnabled)
                BinarizeImage(treshold);
            else
                BinarizeImage(improvedThreshold);

            return GetBitmap(binarizedImage);
        }

        private int ImprovedOtsu(int[,] hists, int total)
        {
            var result = 0;
            var maximum = 0.0;
            var threshold = 0;
            int[] helperVec = new int[256];
            int[,] p_0 = new int[256, 256];
            int[,] mu_i = new int[256, 256];
            int[,] mu_j = new int[256, 256];
            var helper = 120;

            var mu_t0 = 0.0;
            var mu_t1 = 0.0;

            for (int i = 1; i < 256; i++)
            {
                for (int j = 1; j < 256; j++)
                {
                    mu_t0 += i * hists[i, j];
                    mu_t1 += j * hists[j, i];
                }
            }

            for (int ii = 1; ii < 256; ii++)
            {
                for (int jj = 1; jj < 256; jj++)
                {
                    if (jj == 1)
                    {
                        if (ii == 1)
                        {
                            var p11 = p_0[1, 1];
                            var h11 = hists[1, 1];
                            p_0[1, 1] = hists[1, 1];
                        }
                        else
                        {
                            var p0ii11 = p_0[ii - 1, 1];
                            var hists_ii1 = hists[ii, 1];
                            p_0[ii, 1] = p_0[ii - 1, 1] + hists[ii, 1];

                            var muiii11 = mu_i[ii - 1, 1];
                            mu_i[ii, 1] = mu_i[ii - 1, 1] + (ii - 1) * hists[ii, 1];
                            mu_j[ii, 1] = mu_j[ii - 1, 1];
                        }
                    }
                    else
                    {
                        var p0_ii_jj_1 = p_0[ii, jj - 1];
                        var p0_ii_1_jj = p_0[ii - 1, jj];
                        var p0_ii_1_jj_1 = p_0[ii - 1, jj - 1];
                        var histsiijj = hists[ii, jj];

                        p_0[ii, jj] = p_0[ii, jj - 1] + p_0[ii - 1, jj] - p_0[ii - 1, jj - 1] + hists[ii, jj];
                        mu_i[ii, jj] = mu_i[ii, jj - 1] + mu_i[ii - 1, jj] - mu_i[ii - 1, jj - 1] + (ii - 1) * hists[ii, jj];
                        mu_j[ii, jj] = mu_j[ii, jj - 1] + mu_j[ii - 1, jj] - mu_j[ii - 1, jj - 1] + (jj - 1) * hists[ii, jj];
                    }

                    if (p_0[ii, jj] == 0)
                        continue;

                    if (p_0[ii, jj] == total)
                        break;

                    var tr = (Math.Pow(mu_i[ii, jj] - p_0[ii, jj] * mu_t0, 2) + Math.Pow(mu_j[ii, jj] - p_0[ii, jj] * mu_t0, 2)) / (p_0[ii, jj] * (1 - p_0[ii, jj]));

                    if (tr >= maximum)
                    {
                        threshold = ii;
                        maximum = tr;
                    }
                }
            }

            result = threshold + helper - helper;
            return result;
        }

        private int SimpleOtsu(int[] histogram, int total)
        {
            var threshold = 0.0;
            var sum = 0;
            for (int i = 1; i < 256; ++i)
                sum += i * histogram[i];

            int sumB, wB, wF;
            double mB, mF, max, between, threshold1, threshold2;

            sumB = wB = wF = 0;
            mB = mF = max = between = threshold1 = threshold2 = 0.0;

            for (int i = 0; i < 256; ++i)
            {
                wB += histogram[i];
                if (wB == 0)
                    continue;
                wF = total - wB;
                if (wF == 0)
                    break;

                sumB += i * histogram[i];
                mB = sumB / wB;
                mF = (sum - sumB) / wF;
                between = wB * wF * (mB - mF) * (mB - mF);
                if (between >= max)
                {
                    threshold1 = i;
                    if (between > max)
                    {
                        threshold2 = i;
                    }
                    max = between;
                }
            }

            threshold = (threshold1 + threshold2) / 3;
            return (int)threshold;
        }

        private int[] GetHistogram(int[,] img)
        {
            var result = new int[256];

            for (int y = 1; y < height - 1; y++)
            {
                for (int x = 1; x < width - 1; x++)
                {
                    result[img[y, x]]++;
                }
            }
            return result;
        }

        private int[,] Get2DHistogram(int[,] img)
        {
            var histo = new int[256, 256];

            for (int y = 1; y < height - 1; y++)
            {
                for (int x = 1; x < width - 1; x++)
                {
                    var localMean = GetMean(img, y, x);
                    histo[img[y, x], localMean]++;
                }
            }

            return histo;
        }

        private int GetMean(int[,] img, int y, int x)
        {
            var sum = 0;

            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    var val = img[y + i, x + j];
                    sum += val;
                }
            }

            return (int)(sum / 9.0);
        }
    }
}
