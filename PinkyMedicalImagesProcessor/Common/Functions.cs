﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinkyMedicalImagesProcessor.Common
{
    public static class Functions
    {
        public static T Truncate<T>(T val, T min, T max) where T : IComparable
        {
            if (val.CompareTo(min) < 0) return min;
            if (val.CompareTo(max) > 0) return max;
            return val;
        }

        public static byte Truncate(int val)
        {
            return (byte)Truncate(val, 0, 255);
        }
    }
}
