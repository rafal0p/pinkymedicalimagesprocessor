﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinkyMedicalImagesProcessor.Common.Watershed
{
    public class FifoQueue
    {
        List<WatershedPixel> queue = new List<WatershedPixel>();

        public int Count
        {
            get { return queue.Count; }
        }

        public void AddToEnd(WatershedPixel p)
        {
            queue.Add(p);
        }

        public WatershedPixel RemoveAtFront()
        {
            WatershedPixel temp = queue[0];
            queue.RemoveAt(0);
            return temp;
        }

        public bool IsEmpty
        {
            get { return (queue.Count == 0); }
        }

        public override string ToString()
        {
            return base.ToString() + " Count = " + queue.Count.ToString();
        }
    }
}
