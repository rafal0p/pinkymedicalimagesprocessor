﻿using PinkyMedicalImagesProcessor.Common.OCR.KdTree;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinkyMedicalImagesProcessor.Common.OCR
{
    public class TreeContainer : ImageProcessorBase
    {
        private IDictionary<int, IList<int[,]>> learningSamples;

        private int columns = 100;
        private int rowsForDigt = 5;
        private int sampleSize = 20;

        internal void ImportLearningImages(Bitmap bitmap)
        {
            learningSamples = new Dictionary<int, IList<int[,]>>();

            var img = GetImageArrayFromBitmap(bitmap);
            for (int digt = 0; digt < 10; digt++)
            {
                learningSamples.Add(digt, new List<int[,]>());
                for (int digtRow = 0; digtRow < rowsForDigt; digtRow++)
                {
                    var rowStart = digt * digtRow * sampleSize;
                    for (int column = 0; column < columns; column++)
                    {
                        var digtImage = new int[sampleSize, sampleSize];
                        for (int y = 0; y < sampleSize; y++)
                        {
                            var yPos = rowStart + y;
                            var colStart = column * sampleSize;
                            for (int x = 0; x < sampleSize; x++)
                            {
                                var xPos = colStart + x;
                                digtImage[y, x] = img[yPos, xPos];
                            }
                        }
                        learningSamples[digt].Add(digtImage);
                    }
                }
            }

            FillTree(learningSamples);
        }

        private IKdTree<float, int> tree;

        private void FillTree(IDictionary<int, IList<int[,]>> learningSamples)
        {
            tree = new KdTree<float, int>(400, new FloatMath());

            foreach (var digtDicEntry in learningSamples)
            {
                foreach (var sample in digtDicEntry.Value)
                {
                    var pattern1d = Convert2dPatternTo1d(sample);
                    tree.Add(pattern1d, digtDicEntry.Key);
                }
            }
        }

        public int GetDigt(int[,] pattern)
        {
            var pattern1d = Convert2dPatternTo1d(pattern);
            var neighboursList = tree.GetNearestNeighbours(pattern1d, 2);
            var digt = neighboursList.First().Value;
            return digt;
        }

        private float[] Convert2dPatternTo1d(int[,] pattern)
        {
            height = pattern.GetLength(0);
            width = pattern.GetLength(1);
            var result = new float[height * width];
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    result[y * width + x] = pattern[y, x];
                }
            }
            return result;
        }
    }
}
