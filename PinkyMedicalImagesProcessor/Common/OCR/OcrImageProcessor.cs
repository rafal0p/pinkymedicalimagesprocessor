﻿using PinkyMedicalImagesProcessor.Common.CCL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinkyMedicalImagesProcessor.Common.OCR
{
    public class OcrImageProcessor : CclImageProcessor
    {
        public IDictionary<Point, int[,]> PatternsAsArrays { get; private set; }
        private TreeContainer treeContainer;

        public OcrImageProcessor()
        {
            PatternsAsArrays = new Dictionary<Point, int[,]>();
            treeContainer = new TreeContainer();
        }

        public Bitmap GetIntermediateBitmap()
        {
            var resultBitmap = GetBlackBitmap();
            var resultG = Graphics.FromImage(resultBitmap);

            foreach (var pattern in Patterns)
            {
                var minX = pattern.Value.Min(point => point.X);
                var maxX = pattern.Value.Max(point => point.X);
                var minY = pattern.Value.Min(point => point.Y);
                var maxY = pattern.Value.Max(point => point.Y);
                var rawWidth = maxX - minX;
                var rawHeight = maxY - minY;
                var deltaX = 0;
                var deltaY = 0;
                if (rawHeight > rawWidth)
                    deltaX = (rawHeight - rawWidth) / 2;
                else
                    deltaY = (rawWidth - rawHeight) / 2;

                var realWidth = rawWidth + 1 + 2 * deltaX;
                var realHeight = rawHeight + 1 + 2 * deltaY;

                var digtBitmap = new Bitmap(realWidth, realHeight);
                foreach (var point in pattern.Value)
                {
                    var x = point.X - minX + deltaX;
                    var y = point.Y - minY + deltaY;
                    digtBitmap.SetPixel(x, y, Color.FromArgb(point.Value, point.Value, point.Value));
                }

                var digtBitmapScaled = new Bitmap(digtBitmap, 18, 18);
                var digtBitmapWithBorder = new Bitmap(20, 20);

                var g = Graphics.FromImage(digtBitmapWithBorder);
                g.FillRectangle(new SolidBrush(Color.Black), 0, 0, 20, 20);
                g.DrawImage(digtBitmapScaled, 1, 1);

                var patternArray = GetImageArrayFromBitmap(digtBitmapWithBorder);
                PatternsAsArrays.Add(new Point(minX, minY), patternArray);

                resultG.DrawImage(digtBitmapWithBorder, minX, minY);
            }
            return resultBitmap;
        }

        internal Bitmap GetFinalBitmap()
        {
            var result = GetBlackBitmap();
            var g = Graphics.FromImage(result);
            foreach (var pattern in PatternsAsArrays)
            {
                var digt = treeContainer.GetDigt(pattern.Value);
                g.DrawString(digt.ToString(), new Font("Arial", 12), Brushes.Aqua, pattern.Key.X, pattern.Key.Y);
            }
            return result;
        }

        internal void ImportLearningImages(Bitmap learnigSamples)
        {
            treeContainer.ImportLearningImages(learnigSamples);
        }

        private Bitmap GetBlackBitmap()
        {
            var resultBitmap = new Bitmap(width, height);
            var resultG = Graphics.FromImage(resultBitmap);
            resultG.FillRectangle(new SolidBrush(Color.Black), 0, 0, width, height);
            resultG.Dispose();
            return resultBitmap;
        }
    }
}
