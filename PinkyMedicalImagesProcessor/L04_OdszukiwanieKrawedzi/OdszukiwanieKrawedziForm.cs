﻿using PinkyMedicalImagesProcessor.Common.Canny;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PinkyMedicalImagesProcessor.L04_OdszukiwanieKrawedzi
{
    public partial class OdszukiwanieKrawedziForm : Form
    {
        private Bitmap loadedBitmap;
        private CannyImageProcessor cannyProcessor;

        public OdszukiwanieKrawedziForm()
        {
            InitializeComponent();
            button1_Click(this, EventArgs.Empty);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog() { RestoreDirectory = true };
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                loadedBitmap = new Bitmap(ofd.FileName);
                CalculateEdges();
            }
        }

        private void CalculateEdges()
        {
            var lower = float.Parse(lowerTextBox.Text);
            var higher = float.Parse(higherTextBox.Text);
            var sigma = float.Parse(sigmaTextBox.Text);
            cannyProcessor = new CannyImageProcessor();
            cannyProcessor.MaxHysteresisThresh = higher;
            cannyProcessor.MinHysteresisThresh = lower;
            cannyProcessor.Sigma = sigma;
            cannyProcessor.ImportImage(loadedBitmap);
            cannyProcessor.GetResult();
            pixelzoomPictureBox1.Image = cannyProcessor.GetBitmap(cannyProcessor.EdgeMap);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var index = comboBox1.SelectedIndex;
            switch (index)
            {
                case 0:
                    pixelzoomPictureBox1.Image = cannyProcessor.GetBitmap(cannyProcessor.GrayImage);
                    break;
                case 1:
                    pixelzoomPictureBox1.Image = cannyProcessor.GetBitmap(cannyProcessor.FilteredImage);
                    break;
                case 2:
                    pixelzoomPictureBox1.Image = cannyProcessor.GetBitmap(cannyProcessor.DerivativeX);
                    break;
                case 3:
                    pixelzoomPictureBox1.Image = cannyProcessor.GetBitmap(cannyProcessor.DerivativeY);
                    break;
                case 4:
                    pixelzoomPictureBox1.Image = cannyProcessor.GetBitmap(cannyProcessor.Gradient);
                    break;
                case 5:
                    pixelzoomPictureBox1.Image = cannyProcessor.GetBitmap(cannyProcessor.NonMax);
                    break;
                case 6:
                    pixelzoomPictureBox1.Image = cannyProcessor.GetBitmap(cannyProcessor.PostHysteresis);
                    break;
                case 7:
                    pixelzoomPictureBox1.Image = cannyProcessor.GetBitmap(cannyProcessor.GNH);
                    break;
                case 8:
                    pixelzoomPictureBox1.Image = cannyProcessor.GetBitmap(cannyProcessor.GNL);
                    break;
                case 9:
                    pixelzoomPictureBox1.Image = cannyProcessor.GetBitmap(cannyProcessor.EdgeMap);
                    break;
            }
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            CalculateEdges();
        }
    }
}
