﻿namespace PinkyMedicalImagesProcessor.L03_ProgowanieObrazow
{
    partial class ProgowanieObrazowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pixelzoomPictureBox2 = new PinkyMedicalImagesProcessor.Common.PixelzoomPictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.pixelzoomPictureBox1 = new PinkyMedicalImagesProcessor.Common.PixelzoomPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox2)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pixelzoomPictureBox2
            // 
            this.pixelzoomPictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pixelzoomPictureBox2.Location = new System.Drawing.Point(398, 83);
            this.pixelzoomPictureBox2.Name = "pixelzoomPictureBox2";
            this.pixelzoomPictureBox2.Size = new System.Drawing.Size(389, 362);
            this.pixelzoomPictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pixelzoomPictureBox2.TabIndex = 5;
            this.pixelzoomPictureBox2.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.pixelzoomPictureBox2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.button1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.pixelzoomPictureBox1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(790, 448);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(389, 74);
            this.button1.TabIndex = 0;
            this.button1.Text = "Open File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Location = new System.Drawing.Point(398, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(389, 74);
            this.button2.TabIndex = 1;
            this.button2.Text = "Show";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // pixelzoomPictureBox1
            // 
            this.pixelzoomPictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pixelzoomPictureBox1.Location = new System.Drawing.Point(3, 83);
            this.pixelzoomPictureBox1.Name = "pixelzoomPictureBox1";
            this.pixelzoomPictureBox1.Size = new System.Drawing.Size(389, 362);
            this.pixelzoomPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pixelzoomPictureBox1.TabIndex = 4;
            this.pixelzoomPictureBox1.TabStop = false;
            // 
            // ProgowanieObrazowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(790, 448);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ProgowanieObrazowForm";
            this.Text = "ProgowanieObrazowForm";
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox2)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pixelzoomPictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.PixelzoomPictureBox pixelzoomPictureBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private Common.PixelzoomPictureBox pixelzoomPictureBox1;
    }
}