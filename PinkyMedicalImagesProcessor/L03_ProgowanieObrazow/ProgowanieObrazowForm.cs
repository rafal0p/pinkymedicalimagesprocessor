﻿using PinkyMedicalImagesProcessor.Common.Otsu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PinkyMedicalImagesProcessor.L03_ProgowanieObrazow
{
    public partial class ProgowanieObrazowForm : Form
    {
        private Bitmap originalBitmap;
        private OtsuImageProcessor otsuProcessor = new OtsuImageProcessor();

        public ProgowanieObrazowForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Bitmap files (*.bmp)|*.bmp|PNG files (*.png)|*.png|TIFF files (*.tif)|*tif|JPEG files (*.jpg)|*.jpg |All files (*.*)|*.*";
            ofd.FilterIndex = 5;
            ofd.RestoreDirectory = true;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                originalBitmap = new Bitmap(ofd.FileName);
                pixelzoomPictureBox1.Image = originalBitmap;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            otsuProcessor.ImportImage(originalBitmap);
            var result = otsuProcessor.GetResult();
            pixelzoomPictureBox2.Image = result;
        }
    }
}
