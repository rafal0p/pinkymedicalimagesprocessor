﻿using PinkyMedicalImagesProcessor.Common.Canny;
using PinkyMedicalImagesProcessor.Common.Hough;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PinkyMedicalImagesProcessor.L09_WyszukiwanieProstych
{
    public partial class WyszukiwanieProstychForm : Form
    {
        private Bitmap loadedBitmap;
        private HoughImageProcessor houghProcessor;

        public WyszukiwanieProstychForm()
        {
            InitializeComponent();
            button1_Click(this, EventArgs.Empty);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog() { RestoreDirectory = true };
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                loadedBitmap = new Bitmap(ofd.FileName);
                PerformCalculations();
            }
        }

        private void PerformCalculations()
        {
            var lower = int.Parse(lowerTextBox.Text);
            var higher = int.Parse(higherTextBox.Text);
            var sigma = float.Parse(sigmaTextBox.Text);
            var threshold = int.Parse(thresholdTextBox.Text);
            houghProcessor = new HoughImageProcessor();
            houghProcessor.ImportImage(loadedBitmap);
            houghProcessor.MaxThresh = higher;
            houghProcessor.MinThresh = lower;
            houghProcessor.Sigma = sigma;
            houghProcessor.Threshold = threshold;
            var btmp = houghProcessor.GetResult();
            pixelzoomPictureBox1.Image = btmp;
        }

        
        private void calculateButton_Click(object sender, EventArgs e)
        {
            PerformCalculations();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
